---
title: "Sonoff MINI R2"
draft: false
date: 2021-08-31T23:10:08+02:00
description: "A walkthrough to flash tasmota firmware onto a Sonoff MINI R2"
tags: [sonoff, tasmota, diy]
---

# Example circuit diagram

![sonoff_mini_r2.jpg](/en/posts/diy/sonoff_mini_r2.jpg)


# Prerequisits

- Computer connected the the same network as your WiFi Accesspoint
- Second WiFi device (e.g. Android-Phone) to connect the the sonoff WiFi (AP Mode)

{{< callout text="For the *DIY Mode* at least sonoff firmware version 3.3.0 is required! To get the latest update, install the [eWeLink - Smart Home](https://play.google.com/store/apps/details?id=com.coolkit&hl=en&gl=US) App and connect with your sonoff device." >}}

# Enable DIY Mode

1. connect the L~in~ and N~in~ wire
2. turn on the power
3. hold the button for 10 seconds and release
4. the blue WiFi LED should blink 2x fast and 1x slow
5. again, hold the button for 10 seconds and release
6. the blue WiFi LED should blink perpetually fast
7. with the second device:
   1. watch out and connect to a WiFi network like `ITEAD-xxxxxxxxxx`
   2. the password is: `12345678`
   3. in the browser go to: `http://10.10.7.1`
   4. click on `WIFI SETTING`
   5. enter your actual WiFi network credentials
   6. click on `Save`

# Flash Tasmota

{{< callout text="The sonoff device should be in *DIY Mode* and the tools `curl` and `namp` are required." >}}

## Find IP with `nmap`

1. the command looks like this (replace with your subnet): `nmap -sP 200.150.100.0/24` 
2. look out for a new device (run the command once before and after the sonoff device entered the network)
3. save the IP in an environment variable: `SONOFF_IP=200.150.100.160`


## Prepare OTA

{{< callout text="Sonoff OTA flashing allows only a maximum filesize of **504 KB**." >}}

URL Server: `http://sonoff-ota.aelius.com`
URL Image Lite: `http://sonoff-ota.aelius.com/tasmota-latest-lite.bin`

Visit the Server Website and obtain the SHA256 Hash for the `tasmota-latest-lite.bin` image.


## RESTful API with `curl`

Check if the device operates in *DIY Mode* with a simple *info request*

```
curl -XPOST --header "Content-Type: application/json" --data-raw '{"deviceid": "", "data": {}}' http://$SONOFF_IP:8081/zeroconf/info
```

The OTA needs to be unlocked before flashing.

```
curl -XPOST --header "Content-Type: application/json" --data-raw '{"deviceid": "", "data": {}}' http://$SONOFF_IP:8081/zeroconf/ota_unlock
```

Now the tasmota firmware can be flashed. (**Replace the sha256sum** with the currently valid one)

```
curl -XPOST --header "Content-Type: application/json" --data-raw '{"deviceid": "", "data": { "downloadUrl": "http://sonoff-ota.aelius.com/tasmota-latest-lite.bin", "sha256sum": "5c1aecd2a19a49ae1bec0c863f69b83ef40812145c8392eebe5fd2677a6250cc" } }' http://$SONOFF_IP:8081/zeroconf/ota_flash
```

# Configure Tasmota

The newly falshed tasmota device will start in AP (Access Point) Mode.

## Connect the device to your WiFi network

Use a phone to connect to the tasmota WiFi network with a name like `tasmota_xxxxxx-xxxx`. It is not protected and does not require a password. 

An android phone will prompt a sign in message. By selecting the notification the brwoser will redirect to the initial configuration web page. To access the page manually visit `http://192.168.2.1`.

Insert the target WiFi network credentials that the device should connect to and click `Save`. After a few seconds, the new assigned IP address within the target network should be displayed.

## Configure Hardware

1. open the configuration web site of the device
2. go to `Configuration`
3. go to `Configure Template`
   - Name: `Sonoff MINIR2`
   - Based on: `Sonoff Basic (1)`
   - GPIO0: `Button 1`
   - GPIO1: `User`
   - GPIO2: `User`
   - GPIO3: `User`
   - GPIO4: `Switch 1`
   - GPIO5: `None`
   - GPIO9: `None`
   - GPIO10: `None`
   - GPIO12: `Relay 1`
   - GPIO13: `Led_i 1`
   - GPIO14: `None`
   - GPIO15: `None`
   - GPIO16: `None`
   - GPIO17: `None`
   
## Configure MQTT

1. open the configuration web site of the device
2. go to `Configuration`
3. go to `Configure MQTT`
   - enter your mqtt credentials
4. save
5. go to `Configuration`
6. go to `Configure Other`
   - select `MQTT enable`
   - enter device/friendly Name
7. save

## Save/Backup Configuration

It is very useful to have a backup of the configuration. This will also speed up your configuration process if setting up multiple devices. 

1. open the configuration web site of the device
2. go to `Configuration`
3. go to `Backup Configuration`
4. save the file

To restore the configuration (on another device).

1. open the configuration web site of the device
2. go to `Configuration`
3. go to `Restore Configuration`
4. select the saved configuration file
5. click on `Start restore`

{{< callout text="Enjoy!" >}}

# References

- https://tasmota.github.io/docs/Sonoff-DIY/

