---
title: "The lights of the night"
date: 2022-09-20T21:10:59+02:00
draft: false
description: "You know what they say, \"light shines brighter in the dark\".
But sometimes we get fooled by those lights even though we can see crystal clear."
---

Some of those lights shine from far far away and move at a speed so fast, it's hard to grasp. 
But once you observe them, they seem to stay still, motionless.

Others travel the land, as you would do, near the ground, near to you. 
And even though, they pass by slow, once you catch a glimpse they're already gone.

And so it comes, that one night, under the clear dark sky, 
I went outside, and tried to catch this play of light.

Now I am in control and can not be fooled. 
Time is of the essence, but in my favor. 
In fact, I will bend time. 
A period long enough so all those lights can show their play. 
Merged into a single moment, in which their true nature will be revealed.

This captured moment I'll keep for as long as I please. 
And I will take a glimpse every now and then, 
to enjoy this time long gone, anytime, again and again.
 
!["night scene moon and car lights"](/posts/artistic/street_at_night_smal.JPG)
