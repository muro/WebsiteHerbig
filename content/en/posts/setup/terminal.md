---
title: "How the `gnome-terminal` becomes an oasis of well-being"
date: 2022-08-22T22:34:00+02:00
draft: false
description: "In order to feel comfortable on my computer, I have to personalize a few things.
Second item on the agenda, right after the odyssey of choosing the right desktop background,
is setting up the terminal.
I've gotten used to my setup so much that I don't want to be missing it. 
The starting point is a fresh Ubuntu installation."
---

I'm using the `gnome-terminal` on Ubuntu, but the factory settings are not fit for me.

In the heart of my setup beats `zsh`, 
with modifications from `oh-my-zsh` and some personal adjustments.
A minimalistic theme with an appealing color palette must not be missing.
Finally, the GNU Terminal Multiplexer (*tmux*) extension `byobu` finishes my setup
altogether with the integration of the indispensable fuzzy finder `fzf`.

!["screenshot of my final gnome terminal setup"]( /posts/setup/screenshot_gnome_terminal.png )

One dependency that must be met before proceeding with this guide is `git`.

```
$ sudo apt install git
```


## the shell

While the *Z shell* can also be used for scripting, 
it was designed mainly for interactive use. 
Operating the system via the terminal is thus,
even more convenient and efficient.

```
$ sudo apt install zsh
```

Did I mention how incredible customizable *Z shell* is? 
In fact, it is so much that I don't even try to adjust anything myself. 
Instead, I let the *Oh My Zsh* configuration framework do the job for me.

The decent default configuration on its own 
lowers the barrier of entry to *Z shell* a lot,
In addition, the framework provides numerous plugins and themes.

```
% sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

[...]

Time to change your default shell to zsh:
Do you want to change your default shell to zsh? [Y/n] y
Changing your shell to /usr/bin/zsh...

➜  ~ 
```

For more information on *Z shell* and *Oh My Zsh*, see their project pages:
- [zsh.org](https://www.zsh.org/)
- [ohmyz.sh](https://ohmyz.sh/)

### the plugin

In my opinion, the fuzzy finder `fzf` is a **must have** on every system.

```
➜  sudo apt install fzf
```

After the installation all necessary entries for the *Z shell* integration should have been made.

If `fzf` was installed prior to `zsh`, or the integration failed for other reasons,
`fzf` can be added manually to the `plugins` list in the `.zshrc` file.

```
➜  gedit ~/.zshrc

plugins=(git fzf)
```


## the theme

*Oh My Zsh* can easily be extended with themes and plugins.
I use the minimalist **typewritten** theme.
The installation is as simple as `git clone` followed by a few links.

```
➜  git clone https://github.com/reobin/typewritten.git \
             $ZSH_CUSTOM/themes/typewritten

➜  ln -s "$ZSH_CUSTOM/themes/typewritten/typewritten.zsh-theme" \
         "$ZSH_CUSTOM/themes/typewritten.zsh-theme"

➜  ln -s "$ZSH_CUSTOM/themes/typewritten/async.zsh" \
         "$ZSH_CUSTOM/themes/async"
```

The desired theme can be selected by setting the `ZSH_THEME` variable in the `.zshrc` file.

```
➜  gedit ~/.zshrc

ZSH_THEME="typewritten"
```

A small twist of mine turns the symbol with which each line begins with in to a `>`.
This is accomplished by setting the environment variable `TYPEWRITTEN_SYMBOL`.

```
➜  gedit ~/.zshrc

export TYPEWRITTEN_SYMBOL=">"
```

For more information on the *typewritten Theme*, see its project page:
- [typewritten](https://typewritten.dev/#/)


## the colors

The built-in colour themes of the *gnome-terminal* are somewhat ok,
but they don't suit my taste.
With the help of **Gogh**, my favorite colours are always available to me.
A one-liner triggers an interactive dialogue.                 
There, one or more colour palettes can be selected for installation.
I use the colour palette **176 (snazzy)**. 

```
> bash -c  "$(wget -qO- https://git.io/vQgMr)"

[...]

Enter OPTION(S) : 176

Theme: Snazzy
```

For more information on the *Gogh*, see its project page:
- [gogh](https://gogh-co.github.io/Gogh/)


## the multiplexer

In my previous setup, often,
a dozen terminal windows were spread across several virtual desktops.
And if I accessed my machine remotely,
I had to open multiple windows with the same *SSH* connection.

Byobu is like a tiling window manager for the terminal.
The view can be split vertically and horizontally using shortcuts,
and Workspaces can be easily organised into nameable tabs.

```
> sudo apt install byobu
```

*Byobu* provides immense relief, especially if accessed via *SSH*.
The user can log into an existing session 
and find its workspace exactly the way he or she left it.     
                                                                          
It is possible to start `byobu` automatically for each terminal session.  
To do so, the following entries are added to `.profile` and `.zshrc`.

```
> gedit .profile

_byobu_sourced=1 . /usr/bin/byobu-launch 2>/dev/null || true


> gedit .zshrc

source .profile
```

For more information on the *Byobu*, see its project page:
- [byobu.org](https://www.byobu.org/)

> Errors or suggestions for improvement can be submitted by opening an issue on
[codeberg.org](https://codeberg.org/muro/WebsiteHerbig)

