---
title: "A Dev's Journey - Part 00 - Planning"
date: 2023-07-23T12:00:00+02:00
draft: false
description: "Before embarking on my journey, I need a proper plan where I'm heading. 
In this chapter I start with the choice of hardware, the operating system and a skatch for the roadmap."
---

# Preface

In my early days, I was managing multiple devices.
A stationary gaming rig, a mobile gaming laptop and a separate laptop for the more serious stuff.
The idea was to separate concerns.
But it became annoying, to boot another device, just to check on mails, play a game or quickly test some code and commit a flash of inspiration into git. 
Thus I ended up synchronising all devices, so they have all the same applications, configurations and files.
Then however, my mobile devices stayed idle most of the time, since I preferred to use my tower PC on all occasions.
Rarely I had to prepare one of the mobile devices, just to realise how far the states have diverged.

Once again, I want to try to separate concerns. 
But this time much simpler: One _gaming_ device, and one for everything else but gaming.
No stationary devices anymore. Instead an universal docking station for the use at home.


# Hardware

For gaming I will go with the _Steam Deck_.
It is predestined for this slot.
Perfect for gaming on the go, and I already have the _Dock_ for gaming at my desk and on the living room TV.
Having already disposed of my old gaming rig, all that's left is a very lean desktop setup.
But this story is not about _gaming_.

Sadly, neither of my old laptops has USB-C nor any _universal_ docking capabilities (they are old).
Luckily, the perfect candidate is already in the wings. The _Frame Work 13_!
As of today, it features the most recent CPUs from Intel and AMD. 
It's modular, upgradeable and highly repairable.
I have taken the _Frame Work 16_ into consideration, for it has a bigger screen and is more versatile.
But I can do without the expansion bay, and at home the docked setup makes the size irrelevant to me.
Still, on the go I want to be as mobile as possible.
So I go for the smaller size and light weight.

> - Device of choice: Frame Work 13

* [frame.work](https://frame.work)

## Konfiguration

For a few years now, I am an AMD fan.
So I'm really happy to hear, that _frame work_ released their first AMD based system recently. 
As this will become my daily driver for development and stuff, I am not willing to compromise on performance.
Hence, the most performant configuration is the only option.

> - CPU: AMD Ryzen(tm) 7 7840U

Another aspect that I like about _frame work_, is the possibility to bring your own stuff.
I will use this opportunity to buy the memory and storage separately later on. 
Because the _Frame Work 13_ will ship in Q4 of this year, I have enough time to choose and find the appropriate parts.
I'm aiming for 32 GB of RAM and 1 TB of storage.

> - RAM: 32GB (2x 16GB)
> - Storage: 1TB

Next I had to choose my I/O expansion cards. 
The _Frame Work 13_ provides four slots, but I don't need to use all cards at once. 
Spare expansion cards may be exchanged when appropriate.
One USB-C module is mandatory for charging or for the docking station. 
On the go, I would like to have a second USB-C port, so I can connect further gadgets while charging.
USB-A is a must, as nearly every _thumb drive_, my old _yubikey_ and many other devices depend on it.
To prepare _Raspberry Pies_ and download files from my camera I also want to read and write to _Micro SD_ cards.
And my wired headphones need an audio jack.
I pass on the other connector options, since they were rarely used on my old devices anyway. 

> I/O expansion cards:
> - 2x USB-C
> - 1x USB-A
> - 1x Micro SD
> - 1x Audio

I really appreciate the _blank ISO keyboard_ option, as I'm typing the German _bone_ layout and usually need to arrange myself with wrong labeled keys.

> - Keyboard: Blank ISO


# Operating System

It has been over 6 year's now, that I had a Windows dual boot option for gaming purposes.
But even that is not an issue any more, and Windows became obsolete to me.

In 2008 I had my fist experience with a GNU/Linux based operating system.
It was my first job, and this is where my journey into IT and development began.
Back then it was _Debian_.
But shortly after we switched to _Ubuntu_, and since then _Ubuntu_ was my daily driver until today.

Out of curiosity, I've tried different distros every now and then, but none convinced me to seriously consider switching from _Ubuntu_.
The distribution that came closest was _Arch Linux_.
It was really tempting, and I would have chosen _Arch Linux_ to accompany me on the next leg of my journey, if not for recent events. 

For a few years now, there is an underdog working its way up, attracting more and more attention.
I'm taking about _NixOS_ together with the _nix_ package manager!
It's been two years since I've first heard of _nix_, but back then I was to busy.
Few weeks ago _nix_ got in my focus once again, and I went straight for the operating system based on this package manager.

I installed _NixOS_ on my spare laptop, read the installation guide, and focused on the concept.
It reminded me a bit of the _Yocto Project_. 
A declarative operating system based on a single configuration file. 
Packages reference their dependencies, which in turn provide recipes on how to acquire them. 
A chain reaction, which makes installing an entire display manager as simple as typing three lines of code:

```nix
services.xserver.enable = true;
services.xserver.displayManager.gdm.enable = true;
services.xserver.desktopManager.gnome.enable = true;
```

Oh you want KDE instead? Well how about changing the last two lines like this:

```nix
services.xserver.displayManager.sddm.enable = true;
services.xserver.desktopManager.plasma5.enable = true;
```

Don't expect any caveats, because there are none.
Even after changing the desktop manager, there is no clean-up to do.
This is because _nix_ will build a whole new system based exclusively on the content of the configuration file.
Legacy burdens have no effect on the new system.
But in case you screw things up and end up with a miss configured system, _NixOS_ got you covered.
Just boot into a previous (working) _generation_ and you're back on track. 

During runtime the system is immutable, but the `nix-shell` command can temporally provide packages to you.
To have packages permanently available on the system you need to consolidate them within the configuration file.

By now it should be clear: I'm not looking for beginner friendly OS.
There is so much more to _NixOS_, if you are interested better check out [nixos.org](https://nixos.org/) yourself.
But one I can tell: I CHOSE YOU _NixOS_!

> - Operating System: NixOS


# File System and Encryption

Now things begin to be techy. 
Even though, _full disk encryption_ sounds fabulous, it brings some convenience issues that, for me, are not worth the effort.
To leave as little data unencrypted as possible, hence also encrypting `/boot`, an extra `/efi` partition is needed. 
In order to prevent the user of entering the password twice, you need to do some _keyfile_ in _initramfs_ stuff.
As I am typing _bone_ and not _qwerty_, there is even more work to do.

In the end, the key benefit is an encrypted kernel and bootloader.
But as long as the _root_ file system is encrypted, and primarily my personal data, I'm fine.
Hold, there is one more thing I would like to encrypt though, the swap. 
During hibernation, all RAM is written to disk, thus loaded files may be readable.

To be able to hibernate, all RAM needs to fit in the swap area.
I'm planing for 32GB of RAM, so I need 32GB of swap.

Let's talk LVM.
The Logical Volume Management allows simple resizing and reorganizing disks and partitions,
by abstracting then into _logical volumes_ and _volume groups_.
And if you ask _the internet_ for advise, it will overwhelm you with information 'why you should be using it'.
But let's face some facts.
I have the possibility to connect only one physical storage device, on whom I will have one root file system,
which will take up all the space and probably never change.
Just like on all my previous laptops since like forever.
So I pass on the LVM fancy pants.

But I digress, this chapter is about the _file system_ after all. 
As my bootloader I choose the simple `systdemd-boot` which requires `vfat`. 
One down.
For my root file system, I was torn between `ext4`, the stabled default for nearly every Linux based distro for over a decade, 
and `btrfs`, which is denoted unstable and unfinished but is raising in popularity and seems a valid choice for non-Raid purposes. 
If I got it right, the _snapshot_ mechanics of `btrfs` behave like the _generations_ of _NixOS_ but for entire volumes. 
As it states in the `btrfs` documentation, _snapshots_ are NOT _backups_.
I read through the entire feature list, but couldn't find a specific one that convinced me.

As of today, the default go to encryption technology for _Linux_ systems is the _Linux Unified Key Setup_ in its second version (LUKS2).
It is possible to use LUKS2 together with a _YubiKey_, which is the setup that I am striving for. 

Here is a draft of of what my hard drive should look like:

```
+-----------+----------+----------+
|           |          |          |
|  [/boot]  |  [/   ]  |          |
|  vfat     |  ext4    |  swap    |
|  1GB      |  ~977GB  |  32GB    |
|           |          |          |
|           |---------------------|
|           |      L U K S 2      |
+-----------+----------+----------+
```

# Conclusion

Ok, that's it.
I'm done planning.
Even though my _Frame Work 13_ ships at the end of this year, which means several months of waiting,
I will prepare as much as possible for a smooth and quick transition.
Which includes notes, how to's and the preparation of configuration files.
I have high hopes that this will help me set up my new laptop in no time.
Especially the _nix_ and _NixOS_ configuration files which are the main component for system reproduction.
So that every effort put into the configuration will actually be preserved for later.

The ideal scenario for later would be:
1. Set up hard drive
2. Copy the configuration onto the new device
3. Call `nixos-install`
4. Done

Here is how I imagine the roadmap for upcoming post topics (no promises):
- Prepare the Hard Drive w/ LUKS2
- Install NixOS
- Set up Hyprland (window manager)
- Install essential Applications
- Configure zsh
- Configure neovim
- Configure gpg/ssh/yubikey
- Test configuration: wipe system and reproduce

See you next time!
