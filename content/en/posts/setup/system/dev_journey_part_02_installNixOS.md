---
title: "A Dev's Journey - Part 02 - install NixOS"
date: 2023-08-15T12:00:00+02:00
draft: true
description: "The hard drive is prepared. The installation medium is still hot.
Only a few more steps and I will reboot into a flesh install of NixOS."
---

# Preface

This time I will finally install NixOS.
Just mount the prepared partitions, 
set up a minimal startup configuration, 
and reboot.
Shouldn't take long, right?

# Prepare the Mount Points

As a reminder, here is the current state of my partition table:

```
lsblk -o name,size,type,fstype,label
NAME        SIZE TYPE  FSTYPE      LABEL
sda       232.9G disk
+- sda1       1G part  vfat        EFI BOOT
+- sda2   199.9G part  crypto_LUKS
|+-- root 199.9G crypt ext4        root
+- sda3      32G part  crypto_LUKS
+-- swap     32G crypt swap        Linux swap
```

> Check out [Dev's Journey - Part 01](/en/posts/setup/system/dev_journey_part_01_getnixos_preparefs/)
> for how to open a LUKS2 device.

My layout plan is as obvious as it can be. 

```
| NAME    | LABEL      | MOUNT POINT |
|---------|------------|-------------|
| sda2    |            |             |
| +--root | root       | `/`         |
|---------|------------|-------------|
| sda1    | EFI BOOT   | `/boot`     |
|---------|------------|-------------|
| sda3    |            |             |
| +--swap | Linux swap | swap        |
```

Only the order, in which the devices have to be mounted, needs to be observed.
Because `sda1` should reside in `/boot`, the `boot` directory needs to 
be created on `/`, which again needs to be mounted beforehand.
For this purpose I will use `/mnt` as my system root mount point during set up.

The open LUKS2 device can be found in `/dev/mapper/<label>` labeled as provided 
during the `cryptsetup luksOpen` call. 
In my case this is `/dev/mapper/root` for my system root.

```
sudo mount /dev/mapper/root /mnt
```

The `mount` command needs an existing target location to function properly.
So before mounting `sda1`, 
the `boot` directory needs to be created within the future system root.
Remember, that I mounted the system root at `/mnt`. 
Thus the `boot` directory needs to be created relative to this location.

```
sudo mkdir -p /mnt/boot
sudo mount /dev/sda1 /mnt/boot
```

Last but not least, lest assign the swap.

```
sudo swapon /dev/mapper/swap
```

What a nice warm up. Commence with the first invocation of a _nix_ command, shall we?

# Notes:

## Be online

> Skip able if connected otherwise (e.g. Ethernet)

At some point (at the very leas for installation purposes) 
you want to be online. 

> Maybe you are in need of you favorite editor
> or download you prepared configuration file from the web.

```
sudo systemctl start wpa_supplicant
wpa_cli
> scan
> scan_results
> add_network 
0
> set_network 0 ssid "myWifi"
> set_network 0 psk  "wyWifi_password"
> enable_network 0
> save_config
> quit
```

## generate configuration files

```
sudo nixos-generate-config --root /mnt
```

This will generate two files:

- `/mnt/etc/nixos/configuration.nix`
- `/mnt/etc/nixos/hardware-configuration.nix`

##  Set Up Fundamental Settings

Adjust configuration file with some fundamental stuff 
so nixos can be installed and the live usb stick is no longer
of need.

For the following segments I used `vim`, which is delivered 
in the installation image, to edit the configuration file.

### swap auto decrypt

This is a tricky part. First open the `hardware-configuration.nix`
and cut the `swapDevices` section and paste it at the top of the
`configuration.nix` just after the `imports` section.

Under the `swapDevices.device` entry start a new subsection `encrypted`.
This should look something like this:

```
{
  imports = [ ... ]

  swapDevices = [
    device = "/dev/disk/by-uuid/<uuid of swap partition after luksOpen>;
    encrypted = {
      enable = true;
      keyFile = "/mnt-root/keyfile";
      lable = "swap"
      blkDev = "/dev/disk/by-uuid/<uuid of luks encrypted partition containing swap>
    };
  ];

  ...
```

> The mysterious path `/mnt-root` is correct (just think of it as `/`). 
> But don't forget to adjust your keyfile path.

In `vim` to get the `uuid` stuff the following command may come in handy:

```
: r ! lsblk -o name,fstype,uuid
```

Now replace the `<...>` part of the `blkDev` field with the `uuid` 
of the `crypto_LUKS` partition which the parent partition of the swap.

### wifi

```
networking = {
  hostName = "myHostName"
  wireless = {
    enable= true;
    networks.myWifiSSID.pskRaw = "<hash of myWifi password>;
  };
};
```

The `wpa_passphrase` tool can generate the hashed wifi password.

As a vim user you may call the `wpa_passphrase` command from within the editor
and emplace the result just in place like this:

```
: r ! wpa_passphrase myWifeSSID "myWifi_password" | awk 'match($0,/psk=([0-9a-f]+)/,m) { print m[1] }'
```

### user

```
users.users.MyUserName= {
  initialPassword = "changemeonfirstlogin"
  isNormalUser = true;
  extraGroups = ["wheel" "networkmanager"] # Enable sudo fro the user
  packages = with pkgs; [ 
    zsh
    oh-my-zsh
    neovim
    git
    tree
  ];
};
```

### some default apps

```
environment.systemPackages = with pkgs; [
  curl
  wget
];

```

### enable SSH support

```
programs.gnupg.agent = {
  enable = true:
  enableSSHSupport = true;
};

services.openssh.enable = true;
```

## install NixOS

```
sudo nixos-install --root /mnt --no-root-password
sudo reboot
```

Login for the first time with the new user:
- use `initialPassword` from configuration
- call `passwd` and change your password

That's it. Now a minimal NixOS is up and running
and ready to extend upon, next time... ;)

# Notes for part 03
  - some default apps
    - browser
    - terminal
  - choose a desktop -> hyperland
  - choose a login manager -> login to hyperland
  - app launcher -> kickoff?
