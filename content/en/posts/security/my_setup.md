---
title: "My Setup"
date: 2021-05-26T11:23:42+02:00
draft: false
---

I store my keys onto the YubiKey for daily use but I keep backups in case my YubiKey gets damaged or lost, as this will result in data loss and the hassle of creating and distributing new keys to all devices and services.

Here I'll show my key setup.

{{< callout text="It is possible to create the keys directly on the YubiKey itself but then it is impossible to create a backup." >}}

My *PGP Key* is secured with a passphrase and consists of four parts.

- **Main** Certificate 
    - **Sub** Signing
    - **Sub** Encryption
    - **Sub** Authentication
    
The Yubikey holds only the *Sub-Keys* and is protected with a *PIN* which will lock the Yubikey if entered wrong three times in a row.

A second passphrase secures my backup. I desiced to use two backup solutions simultaneously.

- Encrypted USB Thumbdrive
- Encrypted Archive in my private Cloud

![PGP Setup Chart](/en/posts/security/my_pgp_setup.svg)

