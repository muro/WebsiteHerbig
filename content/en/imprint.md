---
title: "Imprint"
date: 2022-01-01T00:00:00+02:00
draft: false
---

## Information according to the Telemediengesetz (TMG) § 5 - Allgemeine Informationspflichten

```
Karl Herbig
Sangstraße 5
57299 Burbach

Telefon: +49 160 / 91 35 45 28
E-Mail: support (at) herbig-burbach.de
```

The Telemediengesetz (TMG) is provided by
the *Bundesministerium der Justiz* and
the *Bundesamt für Justiz* 
on the [gesetze-im-internet.de](https://www.gesetze-im-internet.de/tmg/__5.html) Website.

## Information according to the Medienstaatsvertrag (MStV) § 18 - Informationspflichten und Informationsrechte - Paragraph 2

Responsible for the content:
```
Karl Herbig
Sangstraße 5
57299 Burbach
```

The Medienstaatsvertrag (MStV) is provided by
the *Ministerium des Innern des Landes Nordrhein-Westfalen*
on the [recht.nrw.de](https://recht.nrw.de/lmi/owa/br_bes_text?anw_nr=2&gld_nr=2&ugl_nr=2254&bes_id=42847&menu=0&sg=0&aufgehoben=N&keyword=Medienstaatsvertrag#FN1)
Website.
