---
title: "Datenschutzerklärung"
date: 2022-01-01T00:00:00+02:00
draft: false
---

Personenbezogene Daten (nachfolgend zumeist nur „Daten“ genannt) werden von uns nur 
im Rahmen der Erforderlichkeit sowie zum Zwecke der Bereitstellung eines funktionsfähigen 
und nutzerfreundlichen Internetauftritts, inklusive seiner Inhalte und 
der dort angebotenen Leistungen, verarbeitet.

Gemäß Art. 4 Ziffer 1. der Verordnung (EU) 2016/679, 
also der Datenschutz-Grundverordnung (nachfolgend nur „DSGVO“ genannt), 
gilt als „Verarbeitung“ jeder mit oder ohne Hilfe automatisierter Verfahren 
ausgeführter Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten, 
wie das Erheben, das Erfassen, die Organisation, das Ordnen, die Speicherung, 
die Anpassung oder Veränderung, das Auslesen, das Abfragen, die Verwendung, 
die Offenlegung durch Übermittlung, Verbreitung oder eine andere Form der Bereitstellung, 
den Abgleich oder die Verknüpfung, die Einschränkung, das Löschen oder die Vernichtung.

Mit der nachfolgenden Datenschutzerklärung informieren wir Sie insbesondere über Art, 
Umfang, Zweck, Dauer und Rechtsgrundlage der Verarbeitung personenbezogener Daten, 
soweit wir entweder allein oder gemeinsam mit anderen über die Zwecke und 
Mittel der Verarbeitung entscheiden. 
Zudem informieren wir Sie nachfolgend über die von uns zu Optimierungszwecken sowie 
zur Steigerung der Nutzungsqualität eingesetzten Fremdkomponenten, 
soweit hierdurch Dritte Daten in wiederum eigener Verantwortung verarbeiten.

Unsere Datenschutzerklärung ist wie folgt gegliedert:

1. Informationen über uns als Verantwortliche
2. Rechte der Nutzer und Betroffenen
3. Informationen zur Datenverarbeitung


## 1. Informationen über uns als Verantwortliche

Verantwortlicher Anbieter dieses Internetauftritts im datenschutzrechtlichen Sinne ist:

```
Karl Herbig
Sangstraße 5
57299, Burbach

Telefon: 0160 / 91 35 45 28
E-Mail: support@herbig-burbach.de
```

## 2. Rechte der Nutzer und Betroffenen

Mit Blick auf die nachfolgend noch näher beschriebene Datenverarbeitung haben die Nutzer und Betroffenen das Recht

- auf Bestätigung, ob sie betreffende Daten verarbeitet werden, 
auf Auskunft über die verarbeiteten Daten, 
auf weitere Informationen über die Datenverarbeitung sowie 
auf Kopien der Daten (vgl. auch Art. 15 DSGVO);
- auf Berichtigung oder Vervollständigung unrichtiger bzw. 
unvollständiger Daten (vgl. auch Art. 16 DSGVO);
- auf unverzügliche Löschung der sie betreffenden Daten (vgl. auch Art. 17 DSGVO), 
oder, alternativ, soweit eine weitere Verarbeitung gemäß Art. 17 Abs. 3 DSGVO erforderlich ist, 
auf Einschränkung der Verarbeitung nach Maßgabe von Art. 18 DSGVO;
- auf Erhalt der sie betreffenden und von ihnen bereitgestellten Daten und 
auf Übermittlung dieser Daten an andere Anbieter/Verantwortliche (vgl. auch Art. 20 DSGVO);
- auf Beschwerde gegenüber der Aufsichtsbehörde, sofern sie der Ansicht sind, 
dass die sie betreffenden Daten durch den Anbieter unter Verstoß gegen 
datenschutzrechtliche Bestimmungen verarbeitet werden (vgl. auch Art. 77 DSGVO).

Darüber hinaus ist der Anbieter dazu verpflichtet, alle Empfänger, 
denen gegenüber Daten durch den Anbieter offengelegt worden sind, 
über jedwede Berichtigung oder Löschung von Daten oder die Einschränkung der Verarbeitung, 
die aufgrund der Artikel 16, 17 Abs. 1, 18 DSGVO erfolgt, zu unterrichten. 
Diese Verpflichtung besteht jedoch nicht, soweit diese Mitteilung unmöglich oder 
mit einem unverhältnismäßigen Aufwand verbunden ist. 
Unbeschadet dessen hat der Nutzer ein Recht auf Auskunft über diese Empfänger.

Ebenfalls haben die Nutzer und Betroffenen nach Art. 21 DSGVO das Recht auf 
Widerspruch gegen die künftige Verarbeitung der sie betreffenden Daten, 
sofern die Daten durch den Anbieter nach Maßgabe von Art. 6 Abs. 1 lit. f) DSGVO verarbeitet werden. 
Insbesondere ist ein Widerspruch gegen die Datenverarbeitung zum Zwecke der Direktwerbung statthaft.


## 3. Informationen zur Datenverarbeitung

Ihre bei Nutzung unseres Internetauftritts verarbeiteten Daten werden gelöscht oder gesperrt, 
sobald der Zweck der Speicherung entfällt, 
der Löschung der Daten keine gesetzlichen Aufbewahrungspflichten entgegenstehen und 
nachfolgend keine anderslautenden Angaben zu einzelnen Verarbeitungsverfahren gemacht werden.


### 3.1 Serverdaten

Aus technischen Gründen, insbesondere zur Gewährleistung eines sicheren und stabilen Internetauftritts, 
werden Daten durch Ihren Internet-Browser an uns bzw. an unseren Webspace-Provider übermittelt. 
Mit diesen sog. Server-Logfiles werden u.a. Typ und Version Ihres Internetbrowsers, 
das Betriebssystem, die Website, von der aus Sie auf unseren Internetauftritt gewechselt haben 
(Referrer URL), die Website(s) unseres Internetauftritts, die Sie besuchen, 
Datum und Uhrzeit des jeweiligen Zugriffs sowie die IP-Adresse des Internetanschlusses, 
von dem aus die Nutzung unseres Internetauftritts erfolgt, erhoben.

Diese so erhobenen Daten werden vorrübergehend gespeichert, 
dies jedoch nicht gemeinsam mit anderen Daten von Ihnen.

Diese Speicherung erfolgt auf der Rechtsgrundlage von Art. 6 Abs. 1 lit. f) DSGVO. 
Unser berechtigtes Interesse liegt in der Verbesserung, Stabilität, 
Funktionalität und Sicherheit unseres Internetauftritts.

Die Daten werden spätestens nach sieben Tage wieder gelöscht, 
soweit keine weitere Aufbewahrung zu Beweiszwecken erforderlich ist. 
Andernfalls sind die Daten bis zur endgültigen Klärung eines Vorfalls ganz oder teilweise 
von der Löschung ausgenommen.

#### 3.1.1 uberspace.de

Jonas Pasche, Kaiserstr. 15, 55116 Mainz, Germany

Datenschutzerklärung: https://uberspace.de/de/about/privacy/

### 3.2 codeberg.org

Codeberg e.V., Gormannstraße 14, 10119 Berlin, Germany

Datenschutzerklärung: https://codeberg.org/Codeberg/org/src/branch/main/de/Datenschutz.md 


### 3.3 mastodon.social

Mastodon gGmbH, Mühlenstraße 8a, 14167 Berlin, Germany

Datenschutzerklärung: https://mastodon.social/terms


### 3.4 matirx.org

The Matrix.org Foundation, c/o New Vector Ltd, 10 Queen Street Place, London, United Kingdom, EC4R 1AG

Datenschutzerklärung: https://matrix.org/legal/privacy-notice


# Vorlage für dieses Dokument

[Muster-Datenschutzerklärung](https://www.xn--generator-datenschutzerklrung-pqc.de/) der 
[Anwaltskanzlei Weiß & Partner](https://www.ratgeberrecht.eu/datenschutz/datenschutzerklaerung-generator-dsgvo.html)

