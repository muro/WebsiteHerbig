---
title: "Wie das `gnome-terminal` zur Wohlfühloase wird"
date: 2022-08-22T22:34:00+02:00
draft: false
description: "Damit ich mich an meinem Rechner wohlfühle, muss ich einige Dinge personalisieren.
Punkt zwei auf der Agenda, gleich nach der Odyssee der Wahl des richtigen Desktop-Hintergrundes, 
ist das Einrichten des Terminals.
Ich habe mich so sehr an mein Setup gewöhnt, dass ich es nicht mehr missen möchte.
Ausgangspunkt ist eine frische Ubuntu Installation.
"
---

Ich verwende das `gnome-terminal` unter Ubuntu, aber die Werkseinstellungen sagen mir nicht zu.

In meinem Setup kommt `zsh`, mit den Modifikationen von `oh-my-zsh` 
und einigen persönlichen Anpassungen, zum Einsatz. 
Ein minimalistisches Theme mit ansprechender Farbpalette darf dabei nicht fehlen. 
Finalisiert wird mein Setup mit der GNU Terminal Multiplexer (*tmux*) Erweiterung `byobu` und 
der Integration des unverzichtbaren *fuzzy finder* `fzf`.

!["screenshot of my final gnome terminal setup"]( /posts/setup/screenshot_gnome_terminal.png )

Eine Abhängigkeit, welche zuvor installiert sein muss, bevor mit dieser Anleitung fortgefahren werden kann ist `git`.

```
$ sudo apt install git
```


## die Shell

Die *Z shell* kann zwar auch zum Skripten verwendet werden, 
wurde aber mit dem Fokus auf die interaktive Nutzung entwickelt.
Dadurch gestaltet sich die Bedienung des Systems via Terminal noch komfortabler und effizienter. 

```
$ sudo apt install zsh
```

Habe ich schon erwähnt, wie überwältigend anpassbar die Z shell ist? 
Sogar so sehr, dass ich es gar nicht erst versuche sie selbst anzupassen. 
Stattdessen bediene ich mich am Konfigurations-Framework *Oh My Zsh*. 

Neben einer soliden Basiskonfiguration, 
welche besonders Anfängern den Umstieg zu *Z shell* massiv erleichtert,
stellt des Framework zahlreiche Plugins und Themes bereit.

```
% sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

[...]

Time to change your default shell to zsh:
Do you want to change your default shell to zsh? [Y/n] y
Changing your shell to /usr/bin/zsh...

➜  ~ 
```

Weitere Infos zu *Z shell* und *Oh My Zsh* findest du auf den Projektseiten:
- [zsh.org](https://www.zsh.org/)
- [ohmyz.sh](https://ohmyz.sh/)

### das Plugin

Wie ich finde ein **must have** auf jedem System, der fuzzy finder `fzf`.

```
➜  sudo apt install fzf
```

Die Installation sollte alle notwendigen Einträge zur Integration in die *Z shell* getätigt haben.
War *fzf* bereits vor *zsh* auf dem System installiert, 
oder die Integration hat aus anderen Gründen nicht einwandfrei funktioniert,
kann *fzf* manuell zu den *Plugins* in der `.zshrc` Datei nachgetragen werden.

```
➜  gedit ~/.zshrc

plugins=(git fzf)
```


## das Theme

*Oh My Zsh* kann leicht mit externen Themes erweitert werden. 
Ich verwende das minimalistische **typewritten**. 
Die Installation beschränkt sich auf ein `git clone` gefolgt von ein paar Verlinkungen.

```
➜  git clone https://github.com/reobin/typewritten.git \
             $ZSH_CUSTOM/themes/typewritten

➜  ln -s "$ZSH_CUSTOM/themes/typewritten/typewritten.zsh-theme" \
         "$ZSH_CUSTOM/themes/typewritten.zsh-theme"

➜  ln -s "$ZSH_CUSTOM/themes/typewritten/async.zsh" \
         "$ZSH_CUSTOM/themes/async"
```

Durch das Setzen der `ZSH_THEME` Variable in der `.zshrc` wird das gewünschte Theme aktiviert.

```
➜  gedit ~/.zshrc

ZSH_THEME="typewritten"
```

Eine kleine Anpassung meinerseits 
ändert das Symbol mit dem jede Zeile beginnt zu `>`. 
Zu diesem Zweck wird die Umgebungsvariable `TYPEWRITTEN_SYMBOL` angepasst.

```
➜  gedit ~/.zshrc

export TYPEWRITTEN_SYMBOL=">"
```

Weitere Infos zum Theme:
- [typewritten](https://typewritten.dev/#/)

## die Farben

Die eingebauten Farbpaletten im *gnome-terminal* sind zwar schon ganz ok,
aber mein Favorit ist nicht dabei. 
Mit Hilfe von **Gogh** stehen mir meine Farben jederzeit zu Verfügung.
Ein Einzeiler löst einen interaktiven Dialog aus.
Dort können eine oder mehrere Farbpaletten zu Installation ausgewählt werden.
Ich haben die Farbpalette **176 (snazzy)** in Verwendung.

```
> bash -c  "$(wget -qO- https://git.io/vQgMr)"

[...]

Enter OPTION(S) : 176

Theme: Snazzy
```

Weitere Infos zu *Gogh*:
- [gogh](https://gogh-co.github.io/Gogh/)


## der Multiplexer

In meinem früherem Setup konnten oftmals, 
über mehrere virtuelle Desktops verteilt,
um die zehn Terminal-Fenster gefunden werden. 
Und sobald ich von *Remote* auf meine Maschine zugriff,
musste ich mehrere Fenster mit dem gleichen *SSH* Zugang öffnen.

*Byobu* ist wie ein *Tiling-Window-Manager* für das Terminal. 
Die Anzeige lässt sich mit *Shortcuts* vertikal und horizontal aufteilen und
Arbeitsbereiche lassen sich einfach in benennbaren Tabs organisieren.

```
> sudo apt install byobu
```

Besonders bei Zugriffen via *SSH* ist *Byobu* eine immense Erleichterung.
Der Benutzer kann sich in eine bestehende *Session* einklinken und
findet seinen Arbeitsplatz exakt so vor wie er ihn zurückgelassen hat.

Es ist möglich `byobu` für jede *Terminal-Session* automatisch zu starten.
Dazu werden folgende Einträge in die `.profile` und `.zshrc` nachgetragen. 

```
> gedit .profile

_byobu_sourced=1 . /usr/bin/byobu-launch 2>/dev/null || true


> gedit .zshrc

source .profile
```

Weitere Infos zu *Byobu*:
- [byobu.org](https://www.byobu.org/)

> Findest du Fehler oder hast Verbesserungsvorschläge,
dann öffne ein Ticket auf 
[codeberg.org](https://codeberg.org/muro/WebsiteHerbig)
