---
title: "Impressum"
date: 2022-01-01T00:00:00+02:00
draft: false
---

## Angaben gemäß Telemediengesetz (TMG) § 5 - Allgemeine Informationspflichten

```
Karl Herbig
Sangstraße 5
57299 Burbach

Telefon: +49 160 / 91 35 45 28
E-Mail: support (at) herbig-burbach.de
```

Das Telemediengesetz (TMG) wird 
von dem *Bundesministerium der Justiz* 
und dem *Bundesamt für Justiz* 
auf der Webseite [gesetze-im-internet.de](https://www.gesetze-im-internet.de/tmg/__5.html)
zur Einsicht bereitgestellt.

## Angaben gemäß Medienstaatsvertrag (MStV) § 18 - Informationspflichten und Informationsrechte - Absatz 2

Verantwortlich für Inhalte:
```
Karl Herbig
Sangstraße 5
57299 Burbach
```

Der Medienstaatsvertrag (MStV) wird
vom *Ministerium des Innern des Landes Nordrhein-Westfalen*
auf der Webseite [recht.nrw.de](https://recht.nrw.de/lmi/owa/br_bes_text?anw_nr=2&gld_nr=2&ugl_nr=2254&bes_id=42847&menu=0&sg=0&aufgehoben=N&keyword=Medienstaatsvertrag#FN1)
zur Einsicht bereitgestellt.

